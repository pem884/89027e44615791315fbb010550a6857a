# Assuming you want to connect to "wannaconnect" where a server is listening on port 5000, try the below in PowerShell
# Then connect a client on your machine to 127.0.0.1:5000 - assuming the servers allow this, it should be passed through to the destination server.
$LocalIp="127.0.0.1"; $LocalPort=5000; $ComputerName="wannaconnect"; $RemotePort=5000; $JumpServerUser="myuser"; $JumpServer="canconnect"
ssh -N -L ${LocalIp}:${LocalPort}:${ComputerName}:${RemotePort} ${JumpServerUser}@${JumpServer}